from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone

from .models import todoitem


def home(request):
    todolist = todoitem.objects.filter(todo_done=False).order_by('-pub_date')
    return render(request, 'todo/home.html', {'todolist': todolist})

def home_all(request):
    todolist = todoitem.objects.all().order_by('-pub_date')
    return render(request, 'todo/home.html', {'todolist': todolist})

def create(request):
    if request.method == "POST":
        new_item = todoitem()
        new_item.todo_line = request.POST.get('item_text','')
        new_item.save()
        return redirect('todo:home')
    else:
        return HttpResponse("This is a POST only endpoint.")

@csrf_exempt
def archive(request):
    if request.method == "POST":
        id_of_item_to_archive= request.POST.get('item_to_archive')
        item_to_archive= todoitem.objects.filter(id=id_of_item_to_archive)
        item_to_archive.update(todo_done=True)
        item_to_archive.update(done_date=timezone.now())
        return HttpResponse("OK")
    else:
        return HttpResponse("This is a POST only endpoint.")

def modify(request):
    if request.method == "POST":
        item_to_modify= request.POST.get('item_to_modify')
        item_to_modify_new_line = request.POST.get('item_to_modify_new_line')
        modify_item = todoitem.objects.filter(id=item_to_modify)
        modify_item.update(todo_line=item_to_modify_new_line)
        return redirect('todo:home')
    else:
        return HttpResponse("This is a POST only endpoint.")

@csrf_exempt
def delete(request):
    if request.method == "POST":
        id_of_item_to_delete = request.POST.get('item_to_delete')
        item_to_delete = todoitem.objects.filter(id=id_of_item_to_delete)
        item_to_delete.delete()
        return HttpResponse("OK")
    else:
        return HttpResponse("This is a POST only endpoint.")
