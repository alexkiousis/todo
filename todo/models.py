from django.db import models

class todoitem(models.Model):
    todo_line = models.CharField("A To-Do item", max_length=200)
    pub_date = models.DateTimeField('date published',auto_now_add=True)
    todo_done = models.BooleanField(default=False)
    done_date = models.DateTimeField('date the item was marked as done',blank=True,null=True)
    order = models.PositiveIntegerField(editable=False,db_index=True,default=0)

    def __unicode__(self):
        return self.todo_line
