from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^$',          views.home,     name='home'),
        url(r'^all$',       views.home_all, name='home_all'),
        url(r'^create$',    views.create,   name='create_item'),
        url(r'^delete$',    views.delete,   name='delete_item'),
        url(r'^modify$',    views.modify,   name='modify_item'),
        url(r'^archive$',   views.archive,  name='archive_item'),
    ]
