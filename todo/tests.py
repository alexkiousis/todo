from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from todo.models import todoitem

from django.utils import timezone

def create_todoitem(todoitem_text):
    """Create a todoitem with the text supplied"""
    return todoitem.objects.create(todo_line=todoitem_text,pub_date=timezone.now())

class TodoAppHomeView(TestCase):
    def test_home_view_empty(self):
        """check if empty list returns corrent message"""
        response = self.client.get(reverse('todo:home'))
        self.assertContains(response,"Start your todo list now.")
        self.assertQuerysetEqual(response.context['todolist'],[])

    def test_create_todoitem(self):
        """create an item and check if it is then returned"""
        create_todoitem("Test if a created item shows up on the list.")
        response = self.client.get(reverse('todo:home'))
        self.assertQuerysetEqual(
                response.context['todolist'],
                ['<todoitem: Test if a created item shows up on the list.>']
        )
