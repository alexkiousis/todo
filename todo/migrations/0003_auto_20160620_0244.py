# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0002_auto_20160609_0304'),
    ]

    operations = [
        migrations.AddField(
            model_name='todoitem',
            name='done_date',
            field=models.DateTimeField(null=True, verbose_name=b'date the item was marked as done', blank=True),
        ),
        migrations.AddField(
            model_name='todoitem',
            name='order',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='todoitem',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'date published'),
        ),
    ]
