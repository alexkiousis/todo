from django.contrib import admin

from .models import todoitem

admin.site.register(todoitem)

from django.contrib.auth.models import User, Group

admin.site.unregister(User)
admin.site.unregister(Group)
