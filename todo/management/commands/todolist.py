from django.core.management.base import BaseCommand, CommandError
from todo.models import todoitem

class Command(BaseCommand):
    help = "Output the current list of pending todo items"

    def add_arguments(self, parser):
        parser.add_argument('--all',
                action = 'store_true',
                dest = 'disp_all',
                default = False,
                help = "Display all items."
        )
    def handle(self, *args, **options):
        if not options['disp_all']:
            for item in todoitem.objects.filter(todo_done=False).order_by('-pub_date'):
                self.stdout.write(" - " + item.todo_line)
        else:
            for item in todoitem.objects.all().order_by('-pub_date'):
                if item.todo_done:
                    status = "[done]"
                else:
                    status = "[pending]"
                self.stdout.write(" - " + item.todo_line + " " + status)
