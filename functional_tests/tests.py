from django.test import LiveServerTestCase
# Selenium freks out without static content, so use this version of LiveServer
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        binary = FirefoxBinary('/usr/bin/firefox-esr')
        self.browser = webdriver.Firefox(firefox_binary=binary)
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Open the page
        self.browser.get(self.live_server_url)
        # Basic check for the page title
        self.assertIn('To-Do App',self.browser.title)

        # Check that the main input field exists
        inputbox = self.browser.find_element_by_id('todoitem')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'New item'
        )
        # Send a test item (in order to check if it's displayed)
        inputbox.send_keys('Functional test input 1')
        inputbox.send_keys(Keys.ENTER)
        # Get all printed items and check if the test entry is there
        # You could get away with find_element_by_id to get the first one
        items = self.browser.find_elements_by_id('callout-content')
        self.assertIn('Functional test input 1', [item.text for item in items])
